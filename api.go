package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"bitbucket.org/lookup/request"
)

func main() {
	port, err := getPortFromCommandLine(os.Args)
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/quote/", request.Handler)
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(port), nil))
}

func getPortFromCommandLine(args []string) (int, error) {
	errorMsg := fmt.Sprintf("Usage: '%s PORT' where PORT is the numeric port number to listen on (ex: %s 8080)\n", args[0], args[0])

	if len(args) < 2 {
		return 0, errors.New(errorMsg)
	}

	port, err := strconv.Atoi(args[1])
	if err != nil {
		log.Println(err)
		return 0, errors.New(errorMsg)
	}
	return port, nil
}
