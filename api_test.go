package main

import (
	"testing"
)

func TestPortParameter(t *testing.T) {
	args := []string{"lookup"}
	port, err := getPortFromCommandLine(args)
	if err == nil || port != 0 {
		t.Error("Command line without port number should fail")
	}

	args = []string{"lookup", "8080xxx"}
	port, err = getPortFromCommandLine(args)
	if err == nil || port != 0 {
		t.Error("Non-numeric port parameter should fail")
	}

	args = []string{"lookup", "8080"}
	port, err = getPortFromCommandLine(args)
	if err != nil || port != 8080 {
		t.Error("Valid startup parameters were not handled correctly")
	}
}
