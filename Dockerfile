FROM golang:1.9

WORKDIR /go/src/bitbucket.org/lookup
COPY . .

RUN go install -v bitbucket.org/lookup

EXPOSE 8080

CMD ["lookup","8080"]