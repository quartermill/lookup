package request

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// Handler is the method that processes inbound quote requests.  It assumes that the URL is /quote/SYMBOLS, where
// SYMBOLS is a comma separated list of symbols.
func Handler(w http.ResponseWriter, r *http.Request) {
	symbols := strings.ToUpper(r.URL.Path[len("/quote/"):])
	log.Println(r.URL.Path)

	responseMsg, err := makeQuoteRequest(symbols)
	if err != nil {
		fmt.Fprintf(w, "Error: %s", err)
	} else {
		fmt.Fprint(w, responseMsg)
	}
}

func makeQuoteRequest(symbols string) (string, error) {
	if symbols == "" {
		return "{}", nil
	}

	quoteURL := fmt.Sprintf("https://api.iextrading.com/1.0/stock/market/batch?symbols=%s&types=quote", symbols)
	resp, err := http.Get(quoteURL)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return prettify(body)
}

func prettify(body []byte) (string, error) {
	var pretty bytes.Buffer
	err := json.Indent(&pretty, body, "", "   ")
	if err != nil {
		return "", err
	}
	return string(pretty.Bytes()), nil
}
