package request

import (
	"strings"
	"testing"
)

func TestEmptyQuoteRequest(t *testing.T) {
	response, err := makeQuoteRequest("")
	if err != nil {
		t.Errorf("Empty request returned an error: %s", err)
	}
	if response != "{}" {
		t.Errorf("Expected {} from empty request, not: %s", response)
	}
}

func TestBadSymbol(t *testing.T) {
	response, err := makeQuoteRequest("FOO")
	if err != nil {
		t.Errorf("Bad symbol request returned an error: %s", err)
	}
	if response != "{}" {
		t.Errorf("Expected {} from bad symbol request, not: %s", response)
	}
}

func TestValidSymbol(t *testing.T) {
	response, err := makeQuoteRequest("AAPL")
	if err != nil {
		t.Errorf("valid symbol request returned an error: %s", err)
	}

	index := strings.Index(response, "\"symbol\": \"AAPL\"")
	if index == -1 {
		t.Errorf("Unexpected response from valid symbol: %s", response)
	}

	index = strings.Index(response, "latestPrice")
	if index == -1 {
		t.Errorf("Missing latestPrice from valid symbol response: %s", response)
	}
}
